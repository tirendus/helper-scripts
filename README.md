# README #

### What is this repository for? ###
This repository is created because of laziness. 

System: Ubuntu 16.04

Installs: python3.6, python3.6-dev, build-essentials, uwsgi, nginx, virtualenv, virtualenvwrapper 

Configures: nginx, uwsgi, virtualenvwrapper

Use mkserverinstance to create python-django config files.

### How do I get set up? ###

git clone https://tirendus@bitbucket.org/tirendus/helper-scripts.git

cd helper-scripts && ./gsg_server_essentials.sh && source ~/.bashrc && . /etc/bash_completion.d/gsg_server_autocomplete

mkserverinstance

Afterwards follow instructions on screen

### Who do I talk to? ###

Brick wall