#!/bin/bash

mkserverinstance() {
    read -p "project name(lowercase, no spaces): " proj_name
    read -p "server name (ip address, domain name, space separated if multiple): " server_name
    echo "setting up nginx and uwsgi configs"

    proj_path="/home/$USER/projects/$proj_name"
    env_path="/home/$USER/.envs/$proj_name"
    nginx_config_path="/etc/nginx/sites-available/$proj_name"
    if [ -e "$nginx_config_path" ]
    then
        echo "nginx config for $proj_name already exists - skipping"
    else
sudo tee -a "$nginx_config_path" <<EOF
server {
    listen      80;
    server_name $server_name;

    location /static/ {
        root $proj_path;
    }

    location = /favicon.ico { access_log off; log_not_found off; }

    location / {
        include     uwsgi_params;
        uwsgi_pass  unix:/run/uwsgi/$proj_name.sock;
    }
}
EOF
    fi

    uwsgi_config_path="/etc/uwsgi/sites/$proj_name.ini"
    if [ -e "$uwsgi_config_path" ]
    then
        echo "uwsgi config for $proj_name already exists - skipping"
    else
sudo tee -a "$uwsgi_config_path" <<EOF
[uwsgi]
chdir = $proj_path
home = $env_path
module = $proj_name.wsgi:application

gid = www-data
uid = www-data

master = true
processes = $(($(nproc --all) * 2))

socket = /run/uwsgi/$proj_name.sock
chown-socket = $USER:www-data
chmod-socket = 660
vacuum = true
max-requests = 5000

EOF
    fi
    sudo ln -s "$nginx_config_path" "/etc/nginx/sites-enabled"
    sudo systemctl restart nginx
    echo "nginx config stored at $nginx_config_path"
    echo "uwsgi config stored at $uwsgi_config_path"
}

reloadserverinstance() {
    if [ "$1" != "" ]; then
        proj_name="$1"

        # Collect static
        python_path="/home/$USER/.envs/$proj_name/bin/python3.6"
        manage_path="/home/$USER/projects/$proj_name/manage.py"
        "$python_path" "$manage_path" collectstatic --noinput

        # Reload vassal
        sudo touch --no-dereference /etc/uwsgi/sites/$proj_name.ini
    else
        echo "Please enter project name"
    fi
}