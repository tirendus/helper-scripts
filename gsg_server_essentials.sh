#!/bin/bash

# DO NOT REFORMAT! EOF is not recognized when indented
configVirtualenv() {
if grep -q "VIRTUALENVWRAPPER_PYTHON" ~/.bashrc
then
echo "virtualenvwrapper already configured - skipping"
else
sudo /bin/cat <<EOF >>~/.bashrc

export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.6
export WORKON_HOME=~/.envs
source /usr/local/bin/virtualenvwrapper.sh
EOF
fi
}

configUWSGI() {
sudo tee -a /etc/systemd/system/uwsgi.service <<EOF
[Unit]
Description=uWSGI Emperor service # service description

[Service]
# Makes sure the /run/wsgi directory exists and that ubuntu user owns www-data group
ExecStartPre=/bin/bash -c 'mkdir -p /run/uwsgi; chown ubuntu:www-data /run/uwsgi'
# Point to the uwsgi executable and run in emperor mode to allow multiple applications management from /etc/uwsgi/sites
ExecStart=/usr/local/bin/uwsgi --emperor /etc/uwsgi/sites
Restart=always
KillSignal=SIGQUIT
Type=notify
NotifyAccess=all

[Install]
WantedBy=multi-user.target
EOF
}

addAutocomplete() {
autocomplete_path="/etc/bash_completion.d/gsg_server_autocomplete"
sudo tee "$autocomplete_path" <<EOF
#!/bin/bash
_reloadserverinstance() {
    IFS=\$'\n' tmp=( \$(compgen -W "\$(ls "/home/\$USER/projects")" -- "\${COMP_WORDS[\$COMP_CWORD]}" ))
    COMPREPLY=( "\${tmp[@]// /\ }" )
}
complete -F _reloadserverinstance reloadserverinstance
EOF
}

installEssentials() {

    echo "Installing python and nginx"
    ## Install python
    sudo add-apt-repository -y ppa:jonathonf/python-3.6
    sudo apt -y update
    sudo apt -y upgrade
    sudo apt -y install build-essential python3.6 python3.6-dev python-apt python3-distutils
    sudo apt -y install nginx
    sudo apt-get -y install nodejs
    sudo apt-get -y install npm
    sudo npm install bower -g
    sudo ln -s /usr/bin/nodejs /usr/bin/node
    echo "python and nginx installed"

    echo "Installing pip"
    ## Install pip
    wget https://bootstrap.pypa.io/get-pip.py
    sudo python3.6 get-pip.py
    rm get-pip.py
    (pip -V && pip3 -V && pip3.6 -V) | uniq
    echo "pip installed"

    echo "Installing virtualenv"
    ## Install virtualenv
    sudo -H pip install virtualenv virtualenvwrapper
    configVirtualenv
    echo "virtualenv installed"

    echo "Installing uwsgi"
    sudo -H pip install uwsgi
    sudo mkdir -p /etc/uwsgi/sites
    echo "uwsgi installed"

    echo "Configuring uwsgi service for emperor mode"
    uwsgi_sp="/etc/systemd/system/uwsgi.service"
    if [ ! -e "$uwsgi_sp" ]
    then
        configUWSGI
    else
        echo "uwsgi service config exists - skipping"
    fi
    echo "uwsgi configured"

    echo "Adding sugar (currently only autocomplete)"
    autocomplete_path="/etc/bash_completion.d/gsg_server_autocomplete"
    addAutocomplete
    echo "sugar configured"

    echo "starting and enabing automatic launch for nginx and uwgi"
    sudo systemctl restart nginx
    sudo systemctl enable nginx
    sudo systemctl start uwsgi
    sudo systemctl enable uwsgi
    echo "uwsgi and nginx are running"

    echo "setting up server_setup script"
    sudo cp gsg_server_instance.sh /usr/local/bin/
    if grep -q "gsg_server_instance.sh" ~/.bashrc
    then
    echo "gsg_server_instance already configured - skipping"
    else
    echo "source /usr/local/bin/gsg_server_instance.sh" >> ~/.bashrc
    fi
    echo "server_setup script set up"

    echo "Essentials installation complete. Execute 'source ~/.bashrc && . $autocomplete_path' to activate virtualenvwrapper and gsg_server_instance in the current session."
}

installEssentials

#read -p "Install essentials? (Y/n) " install_essentials
#case $install_essentials in
#    y | Y | yes) installEssentials;;
#esac
